<!-- SPDX-FileCopyrightText: Copyright 2017 Adriaan de Groot <groot@kde.org>
     SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Leaf (LDAP Extra Attribute Filters)

> LDAP is a protocol for data communication, with rich semantics for data
> modelling, security and database administration.  The security is
> security-in-the-large, though, to protect the communications and
> the integrity of the database itself.  Security in-the-small, for
> instance dealing with the privacy of individual attributes, is not
> a concern for most LDAP servers.  Leaf adds LDAP middleware; a
> man-in-the-middle LDAP proxy server that can filter queries and
> responses to ensure the privacy or confidentiality of the attributes
> passing through the middleware.

Leaf builds on the base provided
by [LillyDAP](https://gitlab.com/arpa2/lillydap)
and [Quick DER](https://gitlab.com/arpa2/quick-der)
to provide middleware -- for instance, filters -- that can live between
an LDAP server and the clients of that server.

The Leaf middlewares are all stand-alone executables that perform one
or more tasks in processing LDAP queries; they are transparent to clients
and the server. Middleware executables can be arranged as a pipeline of
LDAP processing, much like a shell pipefile processes text.

## LDAP-MessageLog ##

This middleware executable (based on the ldap-mitm test from LillyDAP)
logs all LDAP messages that pass through it. The messages themselves
are unchanged. Run `messagelog -h` for usage instructions.

## LDAP-QueryLog ##

> NOTE: Not implemented yet

This middleware executable logs queries (LDAP searches) that pass through
it; the search is logged in a human-readable format. The messages themselves
are unchanged.

## LDAP-PrivacyFilter ##

> NOTE: Not implemented yet

This middleware executable modifies the LDAP messages passing through it.
Fields (e.g. object attributes) may be designated *private*, *sensitive*,
or *plain*. By default, fields are plain. Private fields are removed from
the search results returned to a client. Sensitive fields are removed,
unless they match exactly a query value. Plain fields are passed through
unchanged.

As an example, consider an object class Person, with a Dutch social insurance
number attribute BSN. Our example object, Mr. Vincent Oorbeeld, has BSN
123443210.

 - If the field BSN is *private*, then no search query will return any
   data for field BSN.
 - If the field BSN is *sensitive*, then a search query which explicitly
   checks for `(BSN = 123443210)` will return data for field BSN if the
   field matches the queried value.

A query `(sn = Oorbeeld)` returns no BSN values if the BSN field is
*private* or *sensitive*, while `(&(sn = Oorbeeld)(BSN = 123443210))`
returns the BSN value if is is equal to the queried value.

Note that the filtering happens only on the data returned to the client,
and that this filtering may break the schema of the object data returned.

## Examples

These examples assume an LDAP server that uses the ARPA2 example configuration
for an OpenLDAP server. That configuration is insecure: the password
is hard-coded and well-known, for instance.

### LDAP search, bind

In this query, replace the hostname (*db*) in the LDAP URI with the hostname
actually running the database.

```
ldapsearch -H ldap://db/ \
    -w sekreet \
    -D "o=arpa2.net,ou=InternetWide" \
    -b "o=arpa2.net,ou=InternetWide" \
    "(objectclass=person)"
```

This talks directly to the database, and uses bind-authentication, and
does a simple query. If the database is configured with the ARPA2
sample data, then the query returns three (3) entries: John, Robert,
and Adriaan.

To listen in on the conversation, run `messagelog` as follows:

```
messagelog -h db -P 3899 -Db -V
```

This listens on local port 3899 and connects to host *db* -- again, replace
that with the real host name. You can perform the same query as before,
only in the `ldapsearch` command replace `ldap://db/` by `ldap://localhost:3899/`
so that the search talks to messagelog instead of the LDAP server directly.

You will see the same results as before; the messagelog is transparent to
users, but it **has** logged all of the traffic (8 messages in total
for the example search) as binary dumps.

Running `messagelog` again with flag `-Dl` instead of `-Db` will print
an explanation of the LDAP conversation, rather than dumping the conversation
in binary.

### LDAP search, SASL

The same query done above -- with authentication through a bind-DN and a
shared secret -- can be done with SASL as well.

```
ldapsearch -H ldap://db/ \
    -X u:bogus \
    -b "o=arpa2.net,ou=InternetWide" \
    "(objectclass=person)"
```

The example configuration of the LDAP server maps all authentication
requests to the user Robert Smith, who has the password "rJsmitH".
In this example, the authzid passed to the server with the `-X` flag
is "bogus". Because of the example server configuration, this does not
matter and all authzid's are accepted.

The messagelog application can transparently pass through
SASL authentication when run in `-Dl` and `-Db` modes.
