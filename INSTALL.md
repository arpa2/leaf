<!-- SPDX-FileCopyrightText: no
     SPDX-License-Identifier: CC0-1.0
-->

# Build and Install Instructions

> These are instructions for building and installing ARPA2 components
> by hand. If your operating system has packages available for the
> components, you are advised to use those.

## In A Nutshell

Like all ARPA2 (sub-)projects, LEAF has a three-stage build:
 - run `cmake` with suitable arguments
 - run the build-system that is generated
 - run the install step that is generated

For Windows, this is probably different (e.g. all contained within Visual
Studio. On UNIX-like systems, to spell it out,

```
mkdir build
cd build
cmake ..
make
make install
```

This creates a separate build directory (inside the source checkout; you
can also do it somewhere else entirely), runs `cmake` telling it where
the sources are. The default generator for CMake is Makefiles, so
then `make` is the tool to do the build and install steps.

## Dependencies

LEAF uses [CMake](https://cmake.org/) to build; you will need at least
version 3.18. When running cmake, missing dependencies are reported.

### Required

The following ARPA2 software is needed to build LEAF.
These are listed in recommended dependency order, if you are
building the entire stack.
 - *ARPA2CM*, the cmake-support modules for the ARPA2 stack.
 - *arpa2common*, basic libraries and development convenience. Not needed
   directly by LEAF, but required to build Quick DER, below.
 - *Quick MEM*, a memory-pool implementation.
 - *Quick DER*, LDAP-data (DER format) decoding.
 - *Quick SASL*, SASL-management libraries.
 - *LillyDAP*, LDAP-processing framework

### Recommended

The following third-party software is recommended when building LEAF,
for the full range of features and documentation:
 - *Doxygen* produces browsable documentation.
   Debian package: doxygen
   FreeBSD package: devel/doxygen
   openSUSE package:

## Options

LEAF has no CMake options.
