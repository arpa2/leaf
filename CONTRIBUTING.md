<!-- SPDX-FileCopyrightText: no
     SPDX-License-Identifier: CC0-1.0
-->

# Contributing to Leaf

Merge requests are welcome! We would like to accept your contributions
to Leaf, and there are several areas where you can help. Non-exhaustively:

- documentation for the various tools
- translation of documentation
- new tools, or improving existing tools.


## Join the Conversation

ARPA2 as a whole does not have any public instant-messaging or chat channels.

Most project communication goes through the issues-tracker on GitLab.


## Code of Conduct

The ARPA2 community -- of developers, translators, and users --
aims to be courteous, professional, and inclusive. Harrassment, discriminatory
statements and abuse are not tolerated. In general, we apply the
[KDE Code of Conduct](https://www.kde.org/code-of-conduct/) and the
[GNOME Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct) (the
rules of decent behavior in both communities are pretty much the same).


## Merge Requests

Please fork Leaf on GitLab and then submit a merge request. It is recommended
to do a merge request **early** and tag it with *Draft* or *WIP* (work-in-progress)
so that we know you are interested in something.

If you submit a merge request, please mention one of the maintainers in the
comments of the merge request so that a notification gets sent out.


## Code Requirements

> When you write code for Leaf, we ask you to abide by the requirements
> of the repository. We choose these requirements so that everyone can
> contribute and everyone can use the modules. There are also conventions
> for the repository. These are also chosen so that we have, as much as
> possible, consistent code throughout the repository.

### LICENSE

The license for code is, in general [BSD-2-Clause](LICENSE). Please use that
for your own contributions as well. Do add a copyright notice if applicable.
Documentation is usually CC-BY-SA-4.0 or CC0. If you need a different
license, please make that clear in your merge requests.

Leaf applies the [reuse](https://reuse.software/) tool for license-checking
and uses SPDX tags throughout. With the reuse tool, you can easily check if the
repository with your contributions is properly-tagged with license information:

```
$ reuse lint
.. (output snipped) ..
* Files with copyright information: 52 / 52
* Files with license information: 52 / 52

Congratulations! Your project is compliant with version 3.0 of the REUSE Specification :-)
```


### CMake Style

Leaf has inherited its general CMake style from KDE Frameworks (KDE extra-
cmake-modules in particular), but for overall consistency we apply the
(gersemi)[https://github.com/BlankSpruce/gersemi) formatting tool
to the codebase. There is a `.gersemirc` file with the specific
settings for this project. Use gersemi 0.7.5 or later.

Please run `gersemi` on source-files (CMakeLists.txt) before submitting
them. This ensures that all of the build-system-files have the same overall
style. Feel free to apply other styles while doing your own development,
as long as the code that ends up in the repository is consistent.


### C and C++ Style

Leaf has inherited a C and C++ style from KDE Frameworks, which in turn
roughly matches the WebKit style (from when WebKit forked from KDE code).
For overall consistency we use a `.clang-format` file and the clang-format
tool (version 13 preferred).

Please run `clang-format -i -style=file` on source-files (C, C++ and headers)
before submitting them. This ensures that all of the code has the same overall
style. Feel free to other styles while doing your own development,
as long as the code that ends up in the repository is consistent.

There have been some large-scale-reformattings in the past; the file
`.git-blame-ignore-revs` lists the relevant revisions, and you can use
a command-line option, or set a config option (for this repository)

```
$ git blame --ignore-revs-file=.git-blame-ignore-revs ...
$ git config blame.ignoreRevsFile .git-blame-ignore-revs
```

to get a better "git blame" view without the reformatting-revisions.
