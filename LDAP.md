<!-- SPDX-FileCopyrightText: Copyright 2017 Adriaan de Groot <groot@kde.org>
     SPDX-License-Identifier: CC-BY-SA-4.0
-->

# LDAP Setup Guide for LEAF Examples

> This guide is a simple (and even simplistic) guide to setting
> up an LDAP server so that it can be used with LEAF experiments.

## LDAP (bind authentication)

The ARPA2 *howto* repository contains sample data and a sample
configuration for an LDAP server that runs with bind-authentication
only (no SASL) and a known password and bind-DN. It is entirely
insecure, but used in most of the LEAF and other ARPA2-related
examples.

https://gitlab.com/arpa2/howto/-/tree/master/ldap

## LDAP (SASL authentication)

### FreeBSD installation

- Install software: `pkg install cyrus-sasl`
- Add user: `saslpasswd2 -c arpa2` (recommended is to use password "sekreet")
-



- Ensure the running LDAP server has SASL enabled; some packages of
  openldap24-server on FreeBSD may have SASL switched off.
  - A quick way to check is `ldd /usr/local/libexec/slapd` which should
    mention `libsasl2`.
  - Running a SASL-enabled search may indicate "SASL not enabled", e.g.
    query `ldapsearch -H  ldap://db/ -Y DIGEST-MD5`.
- Check that these lines are present in the configuration of the LDAP server:
  ```
  # Set ldap/identithub.arpa2@ARPA2.NET as our PrincipalName@Realm
  sasl-host identityhub.arpa2
  sasl-realm ARPA2.NET
  ```
