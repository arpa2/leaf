/*
 * Log an LDAP conversation.
 *
 * This code connects to an LDAP server and then listens to
 * another port; all data received is passed to the (real)
 * LDAP server, and also dumped to output files (one for
 * input data, one for output data).
 *
 * Typical usage:
 *
 * Suppose you have an LDAP server at db.example.com:389, so
 * that this query returns something:
 *     ldapsearch -h db.example.com -p 389 '(objectclass=device)'
 * It's important that you don't use TLS or similar encryption,
 * because this tool currently doesn't break into that.
 *
 * To man-in-the-middle log a query to that LDAP server, run
 * messagelog with the same -h and -p flags, while giving
 * -H and -P flags for where it should listen; these default
 * to localhost and 3899.
 *     messagelog -h db.example.com -p 389 -H localhost -P 3899
 * Now the entire conversation of a search can be logged to
 * files by running the query against the new listening server:
 *     ldapsearch -h localhost -p 3899 '(objectclass=device)'
 *
 * The MITM server quits after handling a single conversation.
 * Each chunk of data (e.g. LDAPMessage) is dumped to its own
 * file, numbered serially from 0, and named msg.<serial>.<fd>.bin,
 * the fd can be used to distinguish data from client (i.e. ldapsearch)
 * from data from the server (i.e. the real LDAP server). Generally
 * the conversation is started by the client, so msg.000000.<fd>.bin
 * will tell you which one is the client; if you don't have file-
 * descriptor randomisation in the kernel, 5 is usually the client
 * and 3 is the server.
 *
 * Each individual LDAP message is logged to a separate file.
 */

/*
 *  SPDX-FileCopyrightText: Copyright 2017, Adriaan de Groot <groot@kde.org>
 *  SPDX-License-Identifier: BSD-2-Clause
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> /* getopt */

#include "dump-der.h"
#include "dump-opcode.h"
#include "lib/engine.h"
#include "lib/network.h"

/* Print usage string and exit with an error. */
void usage()
{
    fprintf(stderr, "\nUsage: messagelog [-h dsthost] [-p dstport] [-H lsthost] [-P lstport] [options]\n"
                    "\tdsthost and dstport specify the target host and port, like options\n"
                    "\t-h and -p for ldapsearch(1).\n\n"
                    "\tlsthost and lstport specify the hostname and port to listen on.\n"
                    "\tThen use those values as -h and -p for ldapsearch(1) instead.\n\n"
                    "\tOPTIONS\n"
                    "\t-Db\tdump binary (DER-level) messages\n"
                    "\t-Dl\tdump LDAP (opcode-level) messages\n"
                    "\t-V\tVerbose output\n"
                    "\n");
    exit(1);
}

int main(int argc, char **argv)
{
    char *hflag = NULL; /* -h, hostname of server */
    int portval = 389; /* -p, port of server */
    char *ownhflag = NULL; /* -H, hostname for self */
    int ownportval = 3899; /* -P, port for self */
    char style = 'b'; /* style to dump */
    bool verbose = false;
    static const char acceptable_style[] = "bl";

    static const char localhost[] = "localhost";

    int ch;
    while ((ch = getopt(argc, argv, "h:p:H:P:D:V")) != -1) {
        switch (ch) {
        case 'p':
            if (set_port(&portval, optarg) < 0) {
                usage();
            }
            break;
        case 'P':
            if (set_port(&ownportval, optarg) < 0) {
                usage();
            }
            break;
        case 'h':
            hflag = optarg;
            break;
        case 'H':
            ownhflag = optarg;
            break;
        case 'D':
            style = *optarg;
            break;
        case 'V':
            verbose = true;
            break;
        case '?':
        default:
            usage();
        }
    }
    argc -= optind;
    argv += optind;

    if (argc) {
        fprintf(stderr, "Extra command-line arguments ignored.\n");
        usage();
    }
    if (strchr(acceptable_style, style) == NULL) {
        fprintf(stderr, "Dump style '%c' not recognized.\n", style);
        usage();
    }

    int server_fd = connect_server((hflag ? hflag : localhost), portval, 1);
    if (server_fd < 0) {
        usage();
    }

    int client_fd = listen_client((ownhflag ? ownhflag : localhost), ownportval, 1);
    if (client_fd < 0) {
        close(server_fd);
        usage();
    }

    struct LillyStructural *structure = NULL;
    switch (style) {
    case 'b':
        structure = der_dump_configuration();
        break;
    case 'l':
        structure = opcode_dump_configuration();
        break;
    }
    if (NULL == structure) {
        /* This means there is a mismatch between the switch() and
         * the array acceptable_style, because it should have failed already.
         */
        fprintf(stderr, "Dump style '%c' not recognized.\n", style);
        usage();
    }
    run_lilly(server_fd, client_fd, structure, verbose);

    close(client_fd);
    close(server_fd);
    return 0;
}
