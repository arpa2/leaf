/*
 * API for dumping packets at the LDAP-message (opcode) level.
 *
 * Uses the LillyDAP processing stack to do the packet dumping,
 * so that the LillyDAP stack takes care to split things up into
 * individual LDAP messages. By the time we get here, LillyDAP has
 * chopped things up into an LDAP opcode and payload. Currently logs
 * opcodes to stdout.
 *
 */

/*
 *  SPDX-FileCopyrightText: Copyright 2022, Adriaan de Groot <groot@kde.org>
 *  SPDX-License-Identifier: BSD-2-Clause
 */
#ifndef LEAF_MESSAGELOG_DUMP_OPCODE_H
#define LEAF_MESSAGELOG_DUMP_OPCODE_H

struct LillyStructural;

struct LillyStructural *opcode_dump_configuration();

#endif
