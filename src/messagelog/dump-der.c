/*
 *  SPDX-FileCopyrightText: Copyright 2017, Adriaan de Groot <groot@kde.org>
 *  SPDX-License-Identifier: BSD-2-Clause
 */

/* This file implements the dump-binary configuration for `messagelog`.
 * It dumps each message to a serially-numbered file.
 *
 * Running `messagelog -Db` calls into this implementation.
 */

#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

#include <lillydap/api.h>

#include "lib/network.h"
#include "lib/serial.h"

static int lillydump_dercursor(struct LillyConnection *lil, LillyPool qpool, dercursor dermsg)
{
    LDAPX *ldap = (LDAPX *)lil;

    int serialfd = serial_open(ldap);
    if (serialfd < 0) {
        return -1;
    } else {
        if (write_buf(serialfd, (char *)dermsg.derptr, dermsg.derlen, 0) < 0) {
            close(serialfd);
            return -1;
        }
        close(serialfd);
    }

    return lillyput_dercursor(lil, qpool, dermsg);
}

static struct LillyStructural lillydap_dump_put = {
    .lillyget_dercursor = lillydump_dercursor,
    .lillyput_dercursor = lillyput_dercursor,
};

struct LillyStructural *der_dump_configuration()
{
    return &lillydap_dump_put;
}
