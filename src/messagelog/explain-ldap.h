/*
 * Explain (in human-readable form) LDAP messages.
 *
 * This prints an explanation of what a decoded LDAP message means
 * to a given file stream. The explanations are lengthy and multi-
 * line, indended for reading-by-humans.
 */

/*
 *  SPDX-FileCopyrightText: 2017, Adriaan de Groot <groot@kde.org>
 *  SPDX-License-Identifier: BSD-2-Clause
 *
 */

#ifndef LEAF_EXPLAIN_LDAP_H
#define LEAF_EXPLAIN_LDAP_H

#include <stdio.h>

/* Since we're dealing with LDAP, defined in RFC4211, use that header. */
#include <arpa2/quick-der.h>
#include <quick-der/rfc4511.h>

/** @brief Print a SearchRequest message @p rq to @p out
 *
 * Errors are printed to stderr, regardless of @p out, and then returns -1.
 *
 * Returns 0 on success.
 */
int explain_ldap_search_request(FILE *out, const DER_OVLY_rfc4511_SearchRequest *rq);

/* Not implemented
int explain_ldap_search_result(const DER_OVLY_rfc4511_SearchResultEntry* rq);
*/

#endif
