/*
 *  SPDX-FileCopyrightText: Copyright 2022, Adriaan de Groot <groot@kde.org>
 *  SPDX-License-Identifier: BSD-2-Clause
 */

/* This file implements the dump-ldap configuration for `messagelog`.
 * It logs each message (type) to stdout as it passes through.
 *
 * Running `messagelog -Dl` calls into this implementation.
 */

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

#include <lillydap/api.h>
#include <quick-der/rfc4511.h>

#include "lib/memory.h"
#include "lib/network.h"
#include "lib/serial.h"

#include "explain-ldap.h"

/* The following list of opcodes is copied from LillyDAP api.h,
 * slightly edited to replace ; by , . The macro's are redefined
 * to produce nice string output.
 */
#undef nmdop
#undef nonop
#undef nmdov

#define nmdop(x) #x
#define nonop(x) "NOP " #x
#define nmdov(x) #x

static const char *opcode_names[] = {
    nmdop(BindRequest),
    nmdop(BindResponse),
    nmdop(UnbindRequest),
    nmdop(SearchRequest),
    nmdop(SearchResultEntry),
    nmdop(SearchResultDone),
    nmdop(ModifyRequest),
    nmdop(ModifyResponse),
    nmdop(AddRequest),
    nmdop(AddResponse),
    nmdop(DelRequest),
    nmdop(DelResponse),
    nmdop(ModifyDNRequest),
    nmdop(ModifyDNResponse),
    nmdop(CompareRequest),
    nmdop(CompareResponse),
    nmdop(AbandonRequest),
    nonop(17),
    nonop(18),
    nmdop(SearchResultReference),
    nonop(20),
    nonop(21),
    nonop(22),
    nonop(23), /* No use for nmdop(ExtendedRequest)  */
    nonop(24), /* No use for nmdop(ExtendedResponse) */
    nmdop(IntermediateResponse),
    nonop(26),
    nonop(27),
    nonop(28),
    nonop(29),
    nonop(30),
    nonop(31),
    nmdop(StartTLSRequest),
    nmdop(StartTLSResponse),
    nmdop(PasswdModifyRequest),
    nmdop(PasswdModifyResponse),
    nmdov(WhoamiRequest),
    nmdov(WhoamiResponse),
    nmdop(CancelRequest),
    nmdov(CancelResponse),
    nmdop(StartLBURPRequest),
    nmdop(StartLBURPResponse),
    nmdop(EndLBURPRequest),
    nmdov(EndLBURPResponse),
    nmdop(LBURPUpdateRequest),
    nmdov(LBURPUpdateResponse),
    nmdop(TurnRequest),
    nmdov(TurnResponse),
    nmdov(TxnStartRequest),
    nmdov(TxnStartResponse),
    nmdop(TxnEndRequest),
    nmdop(TxnEndResponse),
    nmdov(TxnAbortedNotice),
};
static const size_t opcode_names_count = sizeof(opcode_names) / sizeof(opcode_names[0]);

static void dump_cursor_as_hex(FILE *out, const char *label, dercursor crs)
{
    const int max_bytes_per_line = 16;
    const int max_lines_per_dump = 15;
    const int max_bytes_per_dump = max_bytes_per_line * max_lines_per_dump;

    static const char indent[] = "  .. ";
    static const char leading[] = "        ";

    /* Space for max_bytes written in hex, with spaces, then a gap, then as characters, and a terminating NUL */
    char line_of_bytes[max_bytes_per_line * 3 + 4 + max_bytes_per_line + 1];
    memset(line_of_bytes, ' ', sizeof(line_of_bytes)-1);
    line_of_bytes[sizeof(line_of_bytes)-1] = 0;

    fprintf(out, "%s%s length %zu%s\n", indent, label, crs.derlen, crs.derlen > max_bytes_per_dump ? " (truncated)" : "");

    unsigned long c = 0;
    while (c<crs.derlen && c < max_bytes_per_dump) {
        if ( c > 0 && (c % max_bytes_per_line) == 0) {
            fprintf(out, "%s%s\n", leading, line_of_bytes);
            memset(line_of_bytes, ' ', sizeof(line_of_bytes)-1);
        }
        const int hex_offset = ( c % max_bytes_per_line ) * 3;  /* 2 per digit, and a space */
        const int char_offset = (max_bytes_per_line * 3) + 4 + ( c % max_bytes_per_line );
        const char byte = crs.derptr[c];
        sprintf(line_of_bytes + hex_offset, "%02x", byte);
        line_of_bytes[hex_offset+2] = ' ';
        line_of_bytes[char_offset] = isprint(byte) ? byte : '.';
        c++;
    }
    if (c % max_bytes_per_line != 0) {
        fprintf(out, "%s%s\n", leading, line_of_bytes);
    }
}

static int dump_opcode(
    LDAP *lil,
    LillyPool qpool,
    const LillyMsgId msgid,
    const uint8_t opcode,
    const dercursor operation,
    const dercursor controls)
{
    LDAPX *ldap = (LDAPX *)lil;

    fprintf(stdout, "Message serial=%d id=%d opcode=%s\n", *(ldap->serial), msgid, opcode < opcode_names_count ? opcode_names[opcode] : "<unknown>");
    if (ldap->verbose) {
        if (opcode == 3) {
            /* This is a SearchRequest */
            dercursor c = operation;
            dercursor *data = lillymem_alloc(qpool, sizeof(DER_OVLY_rfc4511_SearchRequest));
            static const derwalk packer[] = { DER_PACK_rfc4511_SearchRequest, DER_PACK_END };
            ;
            int r = der_unpack(&c, packer, data, 1);
            if (0 == r) {
                explain_ldap_search_request(stdout, (DER_OVLY_rfc4511_SearchRequest *)data);
            } else {
                fprintf(stderr, "! Could not unpack search request r=%d.\n", r);
            }
        } else {
            dump_cursor_as_hex(stdout, "LDAP DATA", operation);
        }
    }

    (*(ldap->serial))++;

    return lillyput_opcode(lil, qpool, msgid, opcode, operation, controls);
}

static struct LillyStructural lillydap_structure = {
    .lillyget_dercursor = lillyget_dercursor,
    .lillyput_dercursor = lillyput_dercursor,
    .lillyget_ldapmessage = lillyget_ldapmessage,
    .lillyput_ldapmessage = lillyput_ldapmessage,
    .lillyget_opcode = dump_opcode,
};

struct LillyStructural *opcode_dump_configuration()
{
    return &lillydap_structure;
}
