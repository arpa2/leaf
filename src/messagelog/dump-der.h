/*
 * API for dumping packets at the DER level.
 *
 * Uses the LillyDAP processing stack to do the packet dumping,
 * so that the LillyDAP stack takes care to split things up into
 * individual LDAP messages. Each dumped serial-file contains
 * exactly one message (which could be dumped by hexio, or processed
 * by lilly-dump).
 *
 * There is no entry point; use der_dump_configuration() to get the
 * LillyDAP configuration to pass to the engine (which sets up the processing
 * stack and then waits for data, calling into LillyDAP repeatedly to move data
 * from one side to the other).
 */

/*
 *  SPDX-FileCopyrightText: Copyright 2017, Adriaan de Groot <groot@kde.org>
 *  SPDX-License-Identifier: BSD-2-Clause
 */
#ifndef LEAF_MESSAGELOG_DUMP_DER_H
#define LEAF_MESSAGELOG_DUMP_DER_H

struct LillyStructural;

struct LillyStructural *der_dump_configuration();

#endif
