/*
 *  SPDX-FileCopyrightText: 2017, Adriaan de Groot <groot@kde.org>
 *  SPDX-License-Identifier: BSD-2-Clause
 *
 */

#include "explain-ldap.h"

#include <ctype.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>

/* Forward declarations of more specialized decoding functions. */
int ldapdecode_search(const DER_OVLY_rfc4511_SearchRequest *rq);
int ldapdecode_searchresult(const DER_OVLY_rfc4511_SearchResultEntry *rq);

/*
 * Consider a typical Quick-DER generated typedef,
 *
 *     typedef struct {
 *       DER_OVLY_rfc4511_LDAPDN entry; // LDAPDN
 *       DER_OVLY_rfc4511_AttributeList attributes; // AttributeList
 *     } DER_OVLY_rfc4511_AddRequest;
 *
 * Expanding the typedefs recursively, we'll eventually find out
 * that the first member of member DER_OVLY_rfc4511_AddRequest.entry
 * is a dercursor. Since all the typedefs en up just being, in memory,
 * arrays of dercursor structs, here's a type-casting hammer to smash
 * them to dercursor pointers regardless of how deep the types are
 * nested.
 */
#define DER_CAST_CURSOR(x) ((dercursor *)(&(x)))

/*
 * Recursive decoding of filter expressions. The recursion starts in
 * ldapdecode_filter(), which calls other ldapdecode_*() functions
 * for specialized processing (e.g. for the different operators).
 *
 * Each decoding function takes a dercursor (which points to the bit of
 * DER-encoded filter expression we're working on) and a depth; the
 * depth is used to nicely format the output.
 *
 * Most of the functions also take a label, which is the human-readable
 * name of the operator under consideration, and an indent-string, which
 * is just a convenient way to pass in depth*2 spaces.
 */
static int ldapdecode_filter(FILE *out, dercursor inner, int depth);

/*
 * Binary operators take exactly 2 string arguments (AttributeValueAssertion).
 * They don't recurse.
 */
static int ldapdecode_binop(FILE *out, const char *label, const char *indent, dercursor crs)
{
    uint8_t tag;
    size_t len;
    uint8_t hlen;
    dercursor first = crs;
    if (der_header(&first, &tag, &len, &hlen) < 0) {
        fprintf(stderr, "! Could not read DER header of Filter binary operator element %d.\n", 1);
        return -1;
    }
    if (tag != 0x04) /* String */
    {
        fprintf(stderr, "! Filter binary operator LHS is not a string.\n");
        return -1;
    }
    first.derlen = len;

    dercursor second = crs;
    der_skip(&second);
    if (der_header(&second, &tag, &len, &hlen) < 0) {
        fprintf(stderr, "! Could not read DER header of Filter binary operator element %d.\n", 2);
        return -1;
    }
    if (tag != 0x04) /* String */
    {
        fprintf(stderr, "! Filter binary operator RHS is not a string.\n");
        return -1;
    }
    second.derlen = len;

    fprintf(out, "    %s   (%.*s %s %.*s)\n", indent, (int)first.derlen, first.derptr, label, (int)second.derlen, second.derptr);

    return 0;
}

/*
 * Multi-operators take sequence (or set) of filter expressions to work on.
 * They do recurse.
 */
static int ldapdecode_multiop(FILE *out, const char *label, const char *indent, dercursor inner, int depth)
{
    dercursor iter;
    if (der_iterate_first(&inner, &iter) < 0) {
        fprintf(stderr, "! Could not enter list-operator %s.\n", label);
        return -1;
    }

    fprintf(out, "    %s   (%s\n", indent, label);
    while (der_isnonempty(&iter)) {
        if (ldapdecode_filter(out, iter, depth + 1) < 0) {
            return -1;
        }
        der_iterate_next(&iter);
    }
    fprintf(out, "    %s   )\n", indent);
    return 0;
}

static void ldapprint_list(FILE *out, dercursor *crs)
{
    dercursor iter;
    if (der_iterate_first(crs, &iter) < 0) {
        fprintf(stderr, "! Could not enter list-of.\n");
        return;
    }

    while (der_isnonempty(&iter)) {
        dercursor inner = iter;
        if (der_enter(&inner) == 0) {
            fprintf(out, "        %.*s\n", (int)inner.derlen, inner.derptr);
        }
        der_iterate_next(&iter);
    }
}

/*
 * Decode an entire filter expression, outputting it to out,
 * nicely formated with indentation according to @p depth.
 */
static int ldapdecode_filter(FILE *out, dercursor inner, int depth)
{
    static const char _indent[] = "           ";
    const char *indent = (depth <= 5) ? (_indent + 10 - 2 * depth) : _indent;

#if 0
    /*
     * Another way to decode this is to use the overlay functions
     * from Quick-DER, since this is an ANY construction, we end up
     * with **one** of the overlaid dercursors being filled in.
     *
     * Imagine the function "derp()" checking that the given cursor
     * is not-null with der_isnonempty() (or derptr not NULL).
     */
    dercursor original = inner;
    DER_OVLY_rfc4511_FilterOperation fop;
    static const derwalk packer[] = { DER_PACK_rfc4511_FilterOperation, DER_PACK_END };
    int r = der_unpack(&original, packer, (dercursor*)&fop, 1);
    if (r != 0) {
        fprintf(stderr, "! Could not unpack filter\n");
        return -1;
    }
    derp("and", DER_CAST_CURSOR(fop.andFilter));
    derp("or ", DER_CAST_CURSOR(fop.orFilter));
    derp("not", DER_CAST_CURSOR(fop.notFilter));
    derp("eql", DER_CAST_CURSOR(fop.equalityMatch));
    derp("sub", DER_CAST_CURSOR(fop.substrings));
    derp("geq", DER_CAST_CURSOR(fop.greaterOrEqual));
    derp("leq", DER_CAST_CURSOR(fop.lessOrEqual));
    derp("prs", DER_CAST_CURSOR(fop.present));
    derp("apx", DER_CAST_CURSOR(fop.approxMatch));
#endif

    uint8_t tag;
    size_t len;
    uint8_t hlen;
    /* Read out the tag values (necessary,
     * since the tag distinguishes the CHOICE in Filter semantics)
     * and after that the inner cursor points to the inner
     * data. Note we use the length returned from the der_header()
     * call, not what's in inner.derlen, because when coming from
     * the outer element that derlen is too long (it still counts
     * the whole length of the outer element).
     */
    if (der_header(&inner, &tag, &len, &hlen) < 0) {
        fprintf(stderr, "! Could not read DER header of Filter element.\n");
        return -1;
    }

    switch (tag) {
    case 0xa0: /* and */
        return ldapdecode_multiop(out, "&", indent, inner, depth);
        break;
    case 0xa1: /* or */
        return ldapdecode_multiop(out, "|", indent, inner, depth);
        break;
    case 0xa2: /* not */
        fprintf(out, "    %s (%s\n", indent, "!");
        ldapdecode_filter(out, inner, depth + 1);
        fprintf(out, "    %s )\n", indent);
        break;
    case 0xa3: /* equality */
        return ldapdecode_binop(out, "==", indent, inner);
        break;
    case 0xa5: /* >= */
        return ldapdecode_binop(out, ">=", indent, inner);
        break;
    case 0xa6: /* <= */
        return ldapdecode_binop(out, "<=", indent, inner);
        break;
    case 0x87: /* present? */
        fprintf(out, "    %s?%.*s\n", indent, (int)inner.derlen, inner.derptr);
        break;
    default:
        fprintf(stderr, "! Do not understand filter operation 0x%02x.\n", tag);
        return -1;
    }

    return 0;
}

/*
 * Decode a SearchRequest, outputs some debugging information
 * followed by the search filter in a nicely-formatted way.
 */
int explain_ldap_search_request(FILE *out, const DER_OVLY_rfc4511_SearchRequest *rq)
{
    static const char indent[] = "  .. ";

    /* Unlikely case, since this overlaps with the dercursor we
     * checked in the LDAPMessage to decide to call here.
     */
    if (!rq->baseObject.derptr) {
        fprintf(stderr, "! Missing baseObject.\n");
        return -1;
    }

    /* Interpret the INTEGER attributes as uint, since they
     * are defined as 0..maxInt (2^31 - 1), and interpret
     * ENUMERATED as uint as well.
     */
    uint32_t scope, deref, sizelimit, timelimit, types;
    if (der_get_uint32(rq->scope, &scope) < 0) {
        fprintf(stderr, "! Scope could not be interpreted.\n");
        return -1;
    }
    if (der_get_uint32(rq->derefAliases, &deref) < 0) {
        fprintf(stderr, "! DerefAliases could not be interpreted.\n");
        return -1;
    }
    if (der_get_uint32(rq->sizeLimit, &sizelimit) < 0) {
        fprintf(stderr, "! SizeLimit could not be interpreted.\n");
        return -1;
    }
    if (der_get_uint32(rq->timeLimit, &timelimit) < 0) {
        fprintf(stderr, "! TimeLimit could not be interpreted.\n");
        return -1;
    }
    if (der_get_uint32(rq->typesOnly, &types) < 0) {
        fprintf(stderr, "! TypesOnly could not be interperted.\n");
        return -1;
    }

    fprintf(out, "%sBaseObject='%.*s'\n", indent, (int)rq->baseObject.derlen, rq->baseObject.derptr);
    fprintf(out, "%sScope=%d\n", indent, scope);
    fprintf(out, "%sDerefAliases=%d\n", indent, deref);
    fprintf(out, "%sSizeLimit=%d\n", indent, sizelimit);
    fprintf(out, "%sTimeLimit=%d\n", indent, timelimit);
    fprintf(out, "%sTypesOnly=%d\n", indent, types);

    /*
     * The filter part is labeled "ANY", and is not taken apart into constituent
     * parts. So we'll pick apart the filter items one-by-one,
     * by examining the tags. These are documented in RFC 4511, $4.5.1.
     */
    if (der_isnonempty(&(rq->filter))) {
        fprintf(out, "%sFilter=\n", indent);
        if (ldapdecode_filter(out, rq->filter, 0) < 0) {
            return -1;
        }
    }
    if (der_isnonempty(DER_CAST_CURSOR(rq->attributes))) {
        fprintf(out, "%sAttributes=\n", indent);
        ldapprint_list(out, DER_CAST_CURSOR(rq->attributes));
    }
    return 0;
}

int explain_ldap_search_result(const DER_OVLY_rfc4511_SearchResultEntry *rq)
{
    fprintf(stderr, "! NotImplemented.\n");
    return -1;
}
