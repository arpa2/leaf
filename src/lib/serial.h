/* Since the LDAP structure is passed up and down the processing stack,
 * we can play some memory-allocation tricks with it: we can add private
 * data to the end of the structure by embedding it in a larger (longer)
 * struct, and then casting the LDAP pointers to our own structure
 * type (which is OK as long as we know the pointer we allocate at the
 * beginning is large enough).
 *
 * Here, just add a serial number to the structure. Since we're going
 * to increase the serial number on each packet received, but we have
 * two LillyDAP stacks (one client-to-server, one server-to-client),
 * use a pointer to a shared variable.
 */

/*
 *  SPDX-FileCopyrightText: Copyright 2017, Adriaan de Groot <groot@kde.org>
 *  SPDX-License-Identifier: BSD-2-Clause
 */
#ifndef LEAF_MESSAGELOG_SERIAL_H
#define LEAF_MESSAGELOG_SERIAL_H

#include <lillydap/api.h>

#include <stdbool.h>
#include <stdio.h>

typedef struct {
    struct LillyConnection ldap;
    /* Points to a shared int, so both client and server update the same serial */
    int *serial;
    bool verbose;
} LDAPX;

/** @brief Open a file for the givven message
 *
 * This uses the value pointed-to by serial to come up with a
 * (serial!) filename for the message. It is the caller's responsibility
 * to close the returned file handle.
 *
 * Returns -1 on failure.
 */
int serial_open(LDAPX *ldap);

/** @brief As above, but returns a FILE* (NULL on failure).
 *
 * Use fclose() to close the file.
 */
FILE *serial_open_file(LDAPX *ldap);

#endif
