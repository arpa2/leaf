/*
 *  SPDX-FileCopyrightText: Copyright 2022, Adriaan de Groot <groot@kde.org>
 *  SPDX-License-Identifier: BSD-2-Clause
 */

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

#include <lillydap/api.h>

#include "lib/memory.h"
#include "lib/network.h"
#include "lib/serial.h"

static int pump_lilly(LDAPX *ldap)
{
    int r;
    int zero_reads = 0;

    while ((r = lillyget_event(&ldap->ldap)) > 0) {
        if (ldap->verbose) {
            fprintf(stdout, "(msg.%d) %d -> %d got %d\n", *(ldap->serial), ldap->ldap.get_fd, ldap->ldap.put_fd, r);
        }
    }
    if ((r < 0) && (errno != EAGAIN)) {
        int old_errno = errno;
        perror("get_event");
        fprintf(stdout, "Lilly %d -> %d (msg.%d) failed to get events (r=%d e=%d).\n", ldap->ldap.get_fd, ldap->ldap.put_fd, *(ldap->serial), r, old_errno);
        return r;
    }
    if (0 == r) {
        zero_reads = 1;
    }
    while ((r = lillyput_event(&ldap->ldap)) > 0) {
        if (ldap->verbose) {
            fprintf(stdout, "(msg.%d) %d -> %d put %d\n", *(ldap->serial), ldap->ldap.get_fd, ldap->ldap.put_fd, r);
        }
    }
    if ((r < 0) && (errno != EAGAIN)) {
        int old_errno = errno;
        perror("put_event");
        fprintf(stdout, "Lilly %d -> %d (msg.%d) failed to put events (r=%d e=%d).\n", ldap->ldap.get_fd, ldap->ldap.put_fd, *(ldap->serial), r, old_errno);
        return r;
    }
    if ((r <= 0) && zero_reads) {
        /* 0 bytes were read (by lillyget_event()) and there was nothing to write. */
        return -1;
    }

    return 0;
}

void run_lilly(int server_fd, int client_fd, struct LillyStructural *structure, bool verbose)
{
    /* LillyDAP creates and destroys pools as needed, but we need one
     * for the LDAP structure and some other allocations.
     */
    LillyPool pool = leaf_memory_init();

    /* This is for messages going server -> client */
    LDAPX *ldap_server = lillymem_alloc(pool, sizeof(LDAPX));
    if (!ldap_server) {
        perror("new LDAP server");
        return;
    }
    ldap_server->ldap.def = structure;
    ldap_server->ldap.get_fd = server_fd;
    ldap_server->ldap.put_fd = client_fd;
    ldap_server->verbose = verbose;

    LDAPX *ldap_client = lillymem_alloc(pool, sizeof(LDAPX));
    if (!ldap_client) {
        perror("new LDAP client");
        return;
    }
    ldap_client->ldap.def = structure;
    ldap_client->ldap.get_fd = client_fd;
    ldap_client->ldap.put_fd = server_fd;
    ldap_client->verbose = verbose;

    int serial = 0;
    ldap_server->serial = &serial;
    ldap_client->serial = &serial;
    while (1) {
        fd_set readfds;
        FD_ZERO(&readfds);
        FD_SET(server_fd, &readfds);
        FD_SET(client_fd, &readfds);

        if (select(FD_SETSIZE, &readfds, NULL, NULL, NULL) < 0) {
            perror("select(2):");
            break;
        }

        if (FD_ISSET(server_fd, &readfds)) {
            if (pump_lilly(ldap_server) < 0) {
                break;
            }
        }

        if (FD_ISSET(client_fd, &readfds)) {
            if (pump_lilly(ldap_client) < 0) {
                break;
            }
        }
    }

    lillymem_endpool(ldap_client->ldap.get_qpool);
    lillymem_endpool(ldap_client->ldap.cnxpool);
    lillymem_endpool(ldap_server->ldap.get_qpool);
    lillymem_endpool(ldap_server->ldap.cnxpool);
    lillymem_endpool(pool);
}
