/*
 * Memory-management for LEAF
 *
 * All memory-management in LEAF is delegated to Quick-MEM, which
 * manages pools. To configure LillyDAP to use Quick-MEM, call
 * leaf_memory_init() to get a pool handle (a context, in Quick-MEM
 * terms). That pool handle can be passed to LillyDAP memory functions.
 */

/*
 *  SPDX-FileCopyrightText: Copyright 2017, Adriaan de Groot <groot@kde.org>
 *  SPDX-License-Identifier: BSD-2-Clause
 */
#ifndef LEAF_MEMORY_H
#define LEAF_MEMORY_H

#include <lillydap/mem.h>

/** @brief Configure LillyDAP memory and obtain a pool
 *
 * This sets up LillyDAP memory-management to go through Quick-MEM
 * and returns a new pool handle. Use LillyDAP memory-management
 * functions (e.g. lillydap_alloc()) with this pool.
 */
LillyPool *leaf_memory_init();

#endif
