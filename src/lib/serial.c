/*
 *  SPDX-FileCopyrightText: Copyright 2017, Adriaan de Groot <groot@kde.org>
 *  SPDX-License-Identifier: BSD-2-Clause
 */

#include "serial.h"

#include <fcntl.h>
#include <stdio.h>

int serial_open(LDAPX *ldap)
{
    static char serialfile[64];

    snprintf(serialfile, sizeof(serialfile), "msg.%06d.%d.bin", (*(ldap->serial))++, ldap->ldap.get_fd);
    int serialfd = open(serialfile, O_CREAT | O_WRONLY, 0644);
    if (serialfd < 0) {
        fprintf(stderr, "Could not open data file '%s'.\n", serialfile);
        return -1;
    }

    return serialfd;
}

FILE *serial_open_file(LDAPX *ldap)
{
    int fd = serial_open(ldap);
    if (fd < 0) {
        return NULL;
    }
    return fdopen(fd, "w");
}
