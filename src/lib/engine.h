/*
 * API for driving LillyDAP inside Leaf applications.
 *
 * Uses the LillyDAP processing stack to do packet processing.
 * Sets up generic processing things, then runs a LillyDAP loop
 * with the given structural configuration.
 *
 * Main entry point is run_lilly(), which sets up the processing
 * stack and then waits for data, calling into LillyDAP repeatedly to move data
 * from one side to the other.
 */

/*
 *  SPDX-FileCopyrightText: Copyright 2022, Adriaan de Groot <groot@kde.org>
 *  SPDX-License-Identifier: BSD-2-Clause
 */
#ifndef LEAF_ENGINE_H
#define LEAF_ENGINE_H

#include <stdbool.h>

struct LillyStructural;

void run_lilly(int server_fd, int client_fd, struct LillyStructural *structure, bool verbose);

#endif
