/*
 *  SPDX-FileCopyrightText: Copyright 2017, Adriaan de Groot <groot@kde.org>
 *  SPDX-License-Identifier: BSD-2-Clause
 */

#include <errno.h>
#include <stdio.h>

#include <arpa2/quick-mem.h>
#include <lillydap/api.h>

#include "memory.h"

static void *leaf_newpool()
{
    void *pool = NULL;
    if (!mem_open(0, &pool)) {
        perror("leaf_newpool");
    }
    return pool;
}

static void leaf_endpool(void *context)
{
    mem_close(&context);
}

static void *leaf_alloc(void *context, size_t size)
{
    void *block = NULL;
    if (!mem_alloc(context, size, &block)) {
        perror("leaf_alloc");
    }
    return block;
}

LillyPool *leaf_memory_init()
{
    /* Configure memory allocation functions */
    lillymem_newpool_fun = leaf_newpool;
    lillymem_endpool_fun = leaf_endpool;
    lillymem_alloc_fun = leaf_alloc;

    void *pool = NULL;
    if (!mem_open(0, &pool)) {
        perror("newpool");
        return NULL;
    }

    return pool;
}
