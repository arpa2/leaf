# SPDX-FileCopyrightText: no
# SPDX-License-Identifier: CC0-1.0
#
# Note that Leaf itself is under the BSD-2-Clause license

add_library(
    leaf
    STATIC
    engine.c
    memory.c
    network.c
    serial.c
)
target_link_libraries(leaf PUBLIC ARPA2::LillyDAP-Static)
